import sqlite3
import re

# 從數據庫中獲取文本數據
def get_train_data(k):
    full_data = []
    conn = sqlite3.connect("DB.db")
    cursor = conn.cursor()
    for i in range(k):
        cursor.execute(f"select textBody from article where id={i}") # 從數據庫中選擇文章文本
        textBody = cursor.fetchone()
        if textBody:
            full_data.append(textBody[0])# 如果存在文本數據，將其添加到列表
            
    conn.close()
    return ''.join(full_data)

def preprocess_text(full_data): # 預處理文本數據，僅保留繁體中文字符  
    chinese = r'[\u4E00-\u9FFF]+'
    segments = re.findall(chinese, str(full_data))
    return segments


        
class NGram: #建立模組
    def __init__(self, n, text):
        self.n = n
        self.counters = []
        for _ in range(n+1):
            empty_dict = {}
            self.counters.append(empty_dict)
        self.generate_gram(text)
        self.major_counter = self.counters[n]
        self.minor_counter = self.counters[n-1]
        
    def skip(self, segment, n):
        if len(segment) < n:
            return []
        shift = n - 1
        for i in range(len(segment) - shift):
            yield segment[i : i + shift + 1]
        
    # 生成N-Gram計數器
    def generate_gram(self, text):
        for i in range(1, self.n + 1): # 從1到n的不同N-Gram
            for segment in text: #提取每個list
                for gram in self.skip(segment, i):
                    if gram in self.counters[i]: # 使用skip函數生成N-Gram
                        self.counters[i][gram] += 1
                    else:
                        self.counters[i][gram] = 1
        self.counters[0]['word'] = sum(self.counters[1].values()) #用於儲存所有字元的個數
        
    # 預測下一個詞的概率
    def predict_next_word(self, prefix = '', best = 5):
        if self.n <= 1:
            prefix = 'word'
        else:
            prefix = prefix[-(self.n - 1):] # 獲取前n-1個字符
        count_prefix = self.minor_counter[prefix]
        probs = []    
        for key, count in dict(self.major_counter).items():
            prefix = '' if prefix == 'word' else prefix
            if key.startswith(prefix):
                prob = count / count_prefix
                probs.append((prob, key[-1]))

        sorted_probs = sorted(probs, reverse=True)
        return sorted_probs[:best] if best > 0 else sorted_probs
    
# 創建用於預測文本的類別
class Predict_word:
    def __init__(self, unigram, bigram, trigram):
        self.unigram = unigram
        self.bigram = bigram
        self.trigram = trigram
    
    # 預測一個詞
    def predict_one_word(self, word, best):
        if len(word) == 0:
            prefix = word
            ngram = self.unigram
        elif len(word) == 1:
            prefix = word
            ngram = self.bigram
        else:
            prefix = word[-2:]
            ngram = self.trigram
        return ngram.predict_next_word(prefix, best)
        
    # 預測多個詞
    def predict_n_word(self, word, n, best=1):
        index = 0
        all_word = word
        while index < n:
            next_word = self.predict_one_word(word, best)
            if not next_word:
                word = word[1:]
            else:
                all_word += next_word[0][1]
                word += next_word[0][1]
                index += 1
        return all_word


k = 40 #資料庫提取筆數
full_data = get_train_data(k) #取得資料庫資料
train_data = preprocess_text(full_data) #清洗資料
unigram = NGram(1, train_data) # 基於單個字符的N-Gram模型
bigram = NGram(2, train_data) # 基於前一個字符的N-Gram模型
trigram = NGram(3, train_data) # 基於前兩個字符的N-Gram模型
print(trigram.counters[0]) #所有字元
sorted_dict = sorted(trigram.counters[1].items(), key=lambda item: item[1], reverse=True)
print(sorted_dict[:5]) #每個字符出現頻率，最大的五個
sorted_dict = sorted(trigram.counters[2].items(), key=lambda item: item[1], reverse=True)
print(sorted_dict[:5]) #兩個字符出現頻率，最大的五個
sorted_dict = sorted(trigram.counters[3].items(), key=lambda item: item[1], reverse=True)
print(sorted_dict[:5]) #三個字符出現頻率，最大的五個
model = Predict_word(unigram, bigram, trigram) #建立預測文本的模型
probs = model.predict_one_word('這部電影', 3) #根據前一字元回傳最佳n個選項
print(probs)
essay = model.predict_n_word('電影',20) #創造短文，很明顯沒啥用
print(essay)


